import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <InfoDiv />
    </div>
  );
}

function InfoDiv() {
  const [data, setData] = useState<string|null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string|null>(null);

  useEffect(() => {
    fetch(`/api/info`)
      .then((response) => 
        response.text())
      .then((text) => {
        setData(text);
	setError(null);
      })
      .catch((error) => {
        setError(error.message);
        setData(null);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  return (
    <div className="InfoDiv">
      {loading && <div>loading info div...</div>}
      {error && <div>error loading info div: {error}</div>}
      {data && <div dangerouslySetInnerHTML={{__html: data}} />}
    </div>
  );
}

export default App;
